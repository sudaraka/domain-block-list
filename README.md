# domain-block-list

List of domains that server spam and/or intrusive ads that I use in my
anti-spam/adblock setup.

## Usage

When using (with [DNSCrypt-proxy](https://github.com/DNSCrypt/dnscrypt-proxy))
on your computer/network you can generate a compatible `backlist.txt` file using
the python (3) script provided.

```sh
./generate-dnscrypt-proxy-blacklist.py > /etc/dnscrypt-proxy/custom-blacklist.txt

```

and, add following line to `/etc/dnscrypt-proxy/dnscrypt-proxy.toml`.

`blacklist_file = '/etc/dnscrypt-proxy/custom-blacklist.txt'`

## License

**After 2019-10-07**

Copyright 2019-2020 [Sudaraka Wijesinghe](https://sudaraka.org/)

This program comes with ABSOLUTELY NO WARRANTY;
This is free software, and you are welcome to redistribute it and/or modify it
under the terms of the BSD 2-clause License. See the LICENSE file for more
details.

**Before 2019-10-07**

Copyright 2013-2019 [Sudaraka Wijesinghe](https://sudaraka.org/)

This work was licensed under the Creative Commons Attribution 3.0 Unported
License. To view a copy of this license, visit
[http://creativecommons.org/licenses/by/3.0/](http://creativecommons.org/licenses/by/3.0/).

