#!/usr/bin/env python
# generate-dnscrypt-proxy-blacklist.py: convert raw domain list to DNSCrypt-proxy compatible blacklist
#
# Copyright 2019 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
#
# This program comes with ABSOLUTELY NO WARRANTY
# This is free software, and you are welcome to redistribute it and/or modify
# it under the terms of the BSD 2-clause License. See the LICENSE file for more
# details.
#

from datetime import datetime

import re
import sys

WILDCARD_PATTERNS = { 'ad.*': r'^ad\.'
                    , 'ad[0-9]*': r'^ad[\d]+'
                    , 'ads.*': r'^ads\.'
                    , 'ads[0-9]*': r'^ads[\d]+'
                    , 'count*.*': r'^count.*\.'
                    , 'openad*.*': r'^openad.*\.'
                    , 'openx.*': r'^openx\.'
                    }

TLD_LIST = [ 'ad'
           , 'ar'
           , 'at'
           , 'au'
           , 'be'
           , 'bg'
           , 'biz'
           , 'br'
           , 'ca'
           , 'ch'
           , 'cn'
           , 'co'
           , 'com'
           , 'cz'
           , 'de'
           , 'dk'
           , 'es'
           , 'eu'
           , 'fi'
           , 'fr'
           , 'gr'
           , 'hu'
           , 'co.id'
           , 'co.il'
           , 'in'
           , 'info'
           , 'io'
           , 'it'
           , 'jp'
           , 'co.jp'
           , 'la'
           , 'lk'
           , 'lv'
           , 'ly'
           , 'mobi'
           , 'net'
           , 'nl'
           , 'no'
           , 'nu'
           , 'nz'
           , 'org'
           , 'pl'
           , 'pt'
           , 'ro'
           , 'ru'
           , 'se'
           , 'sg'
           , 'sk'
           , 'to'
           , 'tv'
           , 'tw'
           , 'ua'
           , 'co.uk'
           , 'ltd.uk'
           , 'org.uk'
           , 'us'
           , 'vn'
           , 'vu'
           , 'xyz'
           , 'za'
           ]

domains = {}
wildcards = set()

if '__main__' == __name__:
  raw_file = open('blacklist-raw.txt', 'r')

  # start the read loop
  while 1:
    line = raw_file.readline()

    # stop reading on EOF
    if '' == line:
      break

    # skip comments and blank lines
    if '\n' == line or '#' == line[:1]:
      continue

    dn = line[line.find('"') + 1:line.find('{')].replace('"', '').strip()

    wild_match = [ w for (w, r) in WILDCARD_PATTERNS.items() if re.match(r, dn)]

    if 0 < len(wild_match):
      wildcards.update(wild_match)

      continue

    tld = [v for v in TLD_LIST if v == dn[-1 * len(v):]]

    if 1 > len(tld):
      tld = dn.split('.')[-1]

      print('No matching TLD for:', dn, '(using:', tld + ')', file=sys.stderr)
    else:
      # One or more TLD(s) found.
      # In case of multiple TLD matches, select the longest match

      tld = sorted(tld, key = len, reverse = True)[0]

    if tld not in domains:
      domains[tld] = []

    domains[tld].append(dn)

  raw_file.close()

  # Print header
  print('''
# blacklist.txt: DNSCrypt-proxy blacklist
# Generated: %s
'''.lstrip()
% (datetime.now()))

  # Print applicable block patterns & domain names
  print('''
# Pattern based blocks
%s
'''.strip()
% ('\n'.join(wildcards)))

  for tld in sorted(domains):
    print()
    print('# TLD:', tld)
    print('\n'.join(sorted(domains[tld])))
